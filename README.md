### Usage

---

```go
        import (
                "github.com/rs/zerolog/log"
	        zerologseq "gitlab.com/choskyo/zerolog-seq"
        )

        ...

        seqSink := zerologseq.NewSeqWriter("{address}", "{apiKey}")

	logger := log.Output(seqSink)
```
