package zerologseq

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func TestAppendLevel(t *testing.T) {
	type test struct {
		zeroLevel string
		seqLevel  string
	}

	tests := []test{
		{"trace", "trace"},
		{"debug", "debug"},
		{"info", "information"},
		{"warn", "warning"},
		{"error", "error"},
		{"fatal", "fatal"},
		{"panic", "fatal"},
	}

	sink := NewSeqWriter("", "")

	for testNo, test := range tests {
		buf := bytes.NewBuffer([]byte{})

		sink.appendLevel(buf, test.zeroLevel)

		if buf.String() != fmt.Sprintf(`"@l":"%s"`, test.seqLevel) {
			t.Logf("Buffer: %s", buf.String())
			t.Errorf("%s not found within test %v", test.seqLevel, testNo)
		}
	}
}

func TestAppendTime(t *testing.T) {
	type test struct {
		now      time.Time
		expected string
	}

	tests := []test{
		{
			time.Now().Add(100 * time.Hour),
			time.Now().Add(100 * time.Hour).Format(time.RFC3339),
		},
		{
			time.Now().Add(31337 * time.Millisecond),
			time.Now().Add(31337 * time.Millisecond).Format(time.RFC3339),
		},
		{
			time.Now().Add(-42 * time.Hour),
			time.Now().Add(-42 * time.Hour).Format(time.RFC3339),
		},
	}

	sink := NewSeqWriter("", "")

	for testNo, test := range tests {
		buf := bytes.NewBuffer([]byte{})

		sink.appendTime(buf, test.now)

		if buf.String() != fmt.Sprintf(`"@t":"%s"`, test.expected) {
			t.Logf("Buffer: %s", buf.String())
			t.Errorf("%s not found within test %v", test.expected, testNo)
		}
	}
}

func TestAppendMessage(t *testing.T) {
	type test struct {
		message string
	}

	tests := []test{
		{"bla bla bla"},
		{"en prøve melding"},
		{"赤いリンゴはおいしい"},
	}

	sink := NewSeqWriter("", "")

	for testNo, test := range tests {
		buf := bytes.NewBuffer([]byte{})

		sink.appendMessage(buf, test.message)

		if buf.String() != fmt.Sprintf(`"@m":"%s"`, test.message) {
			t.Errorf("Got: %s, Expected: %s, Test %v", buf.String(), test.message, testNo)
		}
	}
}

func TestAppendCustomFields(t *testing.T) {
	type test struct {
		fields map[string]interface{}
	}

	tests := []test{
		{
			map[string]interface{}{
				"test_number":      0.1234,
				"test_string":      "testy",
				"test_bool":        true,
				"test_bool2":       false,
				"test_stringEmpty": "",
				// "test_time":        time.Now(),
			},
		},
	}

	sink := NewSeqWriter("", "")

	for testNo, test := range tests {
		buf := bytes.NewBuffer([]byte{})

		sink.appendCustomFields(buf, test.fields)

		result := map[string]interface{}{}

		resString := fmt.Sprintf(`{%s}`, buf.String())
		resJson := bytes.NewBufferString(resString)

		err := json.Unmarshal(resJson.Bytes(), &result)
		if err != nil {
			t.Error(err)
			return
		}

		for fieldName, fieldValue := range test.fields {
			if result[fieldName] != fieldValue {
				t.Logf("Buffer: %s", buf.String())
				t.Errorf("Expected: %s, Got: %v in test %v", fieldName, result[fieldName], testNo)
			}
		}

	}
}

// func TestIntegration(t *testing.T) {
// 	sink := NewSeqWriter("http://localhost:5341", "DttMYtkp2avzFu5Km1ZZ")
// 	logger := zerolog.New(sink)

// 	testErr := errors.New("test error message")

// 	testTimestamp := time.Now()

// 	logger.Info().
// 		Bool("test_b", true).
// 		Str("test_str", "qweqwe").
// 		AnErr("error_2", testErr).
// 		Err(testErr).
// 		Float64("test_f64", 0.1234).
// 		Time("test_time", testTimestamp).
// 		Msg("TestIntegration")
// }
