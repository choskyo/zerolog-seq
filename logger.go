package zerologseq

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type SeqWriter struct {
	address string
	apiKey  string
}

func NewSeqWriter(address, apiKey string) SeqWriter {
	w := SeqWriter{
		address: address,
		apiKey:  apiKey,
	}

	return w
}

func (w SeqWriter) Write(data []byte) (int, error) {
	dataMap := make(map[string]interface{})

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.UseNumber()

	err := decoder.Decode(&dataMap)
	if err != nil {
		panic(err)
	}

	buf := bytes.NewBuffer([]byte{})

	buf.WriteString("{")

	w.appendTime(buf, time.Now())
	buf.WriteString(",")
	w.appendLevel(buf, dataMap["level"].(string))
	buf.WriteString(",")
	w.appendMessage(buf, dataMap["message"].(string))
	buf.WriteString(",")
	w.appendCustomFields(buf, dataMap)
	buf.WriteString("}\n")

	w.sendRequest(buf)

	return 1, nil
}

func (w SeqWriter) appendMessage(buf *bytes.Buffer, message string) {
	prop := fmt.Sprintf(`"@m":"%s"`, message)

	buf.WriteString(prop)
}

func (w SeqWriter) appendLevel(buf *bytes.Buffer, zeroLevel string) {
	seqLevel := ""

	switch zeroLevel {
	case "trace":
		seqLevel = "trace"
	case "debug":
		seqLevel = "debug"
	case "info":
		seqLevel = "information"
	case "warn":
		seqLevel = "warning"
	case "error":
		seqLevel = "error"
	case "fatal", "panic":
		seqLevel = "fatal"
	}

	prop := fmt.Sprintf(`"@l":"%s"`, seqLevel)

	buf.WriteString(prop)
}

func (w SeqWriter) appendTime(buf *bytes.Buffer, t time.Time) {
	now := t.Format(time.RFC3339)

	prop := fmt.Sprintf(`"@t":"%s"`, now)

	buf.WriteString(prop)
}

func (w SeqWriter) appendCustomFields(buf *bytes.Buffer, fields map[string]interface{}) {
	desiredFields := map[string]interface{}{}

	for fieldName, fieldValue := range fields {
		switch fieldName {
		case "level", "message":
			continue
		default:
			desiredFields[fieldName] = fieldValue
		}
	}

	fieldsAppended := 0
	totalFields := len(desiredFields)

	for fieldName, fieldValue := range desiredFields {
		var val = fmt.Sprintf("%v", fieldValue)

		switch fieldValue.(type) {
		case string:
			val = fmt.Sprintf(`"%v"`, val)
		}

		fieldBuf := bytes.NewBufferString(fmt.Sprintf(`"%s":%s`, fieldName, val))

		buf.Write(fieldBuf.Bytes())

		if fieldsAppended < totalFields-1 {
			buf.WriteString(",")
			fieldsAppended++
		}
	}

}

func (w SeqWriter) sendRequest(buf *bytes.Buffer) error {
	endpoint := fmt.Sprintf("%s/api/events/raw?clef", w.address)

	req, err := http.NewRequest(http.MethodPost, endpoint, buf)
	if err != nil {
		panic(err)
	}

	req.Header.Set("Content-Type", "application/vnd.serilog.clef")

	if w.apiKey != "" {
		req.Header.Set("X-Seq-ApiKey", w.apiKey)
	}

	_, err = http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}

	return nil
}
